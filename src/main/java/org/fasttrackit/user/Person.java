package org.fasttrackit.user;

public abstract class Person {
    private final String cnp;
    private final String firstName;
    private final String lastName;

    public Person(String cnp, String firstName, String lastName) {
        this.cnp = cnp;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public abstract String getFullName();
}
