package org.fasttrackit.input;

import java.io.InputStream;
import java.util.Scanner;

public class ReadUtils {
    public static int readIntFromKeyboard(String msg) {
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        //sau Scanner keyboard = new Scanner(System.in);
        System.out.println(msg);
        return keyboard.nextInt();
    }

    public static String readFromKeyboard(String msg) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(msg);
        return keyboard.nextLine();
    }

    public static String justReadFromKeyboard() {
        Scanner keyboard = new Scanner(System.in);
        // System.out.println(msg);
        return keyboard.nextLine();
    }
}
