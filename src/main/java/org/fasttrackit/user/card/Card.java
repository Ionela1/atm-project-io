package org.fasttrackit.user.card;

public class Card {
    private final String cardNumber;
    private String pin;

    public Card(String cardNumber, String pin) {
        this.cardNumber = cardNumber;
        this.pin = pin;
    }

    public boolean verifyPin(String inputPin) {
        return this.pin.equals(inputPin);
    }

    public void updatePin(String changedPin) {
        this.pin = changedPin;
    }

    public void setPin(String changedPin) {
        this.pin = changedPin;
    }

    public String getCardNumber() {
        return this.cardNumber;
    }
}
