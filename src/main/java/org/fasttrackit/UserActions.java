package org.fasttrackit;

import org.fasttrackit.output.AtmDispatcher;
import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;

public class UserActions {
    static void withdraw(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        AtmDispatcher.withdraw(someone.getBankAccount(), amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }

    static void changePin(BankCustomer someone) {
        String initialPin = Display.askForPin();
        boolean isValid= someone.validatePin(initialPin);
        if (!isValid){
            Display.displayInvalidPinMsg();
            return;
        }
        String changedPin = Display.askForNewPin();
        String confirmChangedPin = Display.confirmNewPin();
        boolean pinMatches = someone.validateChangedPin(changedPin, confirmChangedPin);
        if (!pinMatches){
            Display.displayInvalidPinMsg();
            return;
        }
        someone.updatePin(changedPin);
        System.out.println("Pin correct. Update pin:"+changedPin+" and try again.");
    }

    static void depositCash(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        someone.depositCash(amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }
   static void deactivateAccount(BankCustomer someone) {
        someone.deactivateAccount();
        Display.informUserWithDeactivation();
    }

}
