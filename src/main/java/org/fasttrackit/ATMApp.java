package org.fasttrackit;

import org.fasttrackit.employee.CLvlEmployee;
import org.fasttrackit.employee.Employee;
import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;

import static org.fasttrackit.UserActions.*;
import static org.fasttrackit.input.UserInput.authenticate;
import static org.fasttrackit.utils.DataUtils.getCustomer;

public class ATMApp {
    public static final String ATM_NAME = "Free Cash ATM";

    public static void main(String[] args) {
        BankCustomer someone = getCustomer();

        Display.showWelcomeMsg();
        boolean passThrough = authenticate(someone);
        if (!passThrough) {
            Display.lockUserFor24h();
            return;
        }

        Display.showMenu();
        String option = someone.selectOptionMenu();
        switch (option) {
            case "0" -> Display.repairAtm(initCLvlEmployee());
            case "1" -> withdraw(someone);
            case "2" -> Display.showCustomerBalanceSheet(someone.getBankAccount());
            case "3" -> changePin(someone);
            case "4" -> Display.activateCardMessage(someone);
            case "5" -> depositCash(someone);
            case "6" -> deactivateAccount(someone);
            case "7" -> Display.getShowIban(someone);
            case "8" -> Display.showCardDetails(someone.getBankAccount().getCard());
        }
    }

    private static Employee initEmployee() {
        return new Employee("1852052326589", "Dorel", "Dorica", "boss");
    }

    private static Employee initCLvlEmployee() {
        return new CLvlEmployee("2869090323981", "Tim", "Cook", "Mr.", "CEO");
    }

}