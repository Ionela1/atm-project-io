package org.fasttrackit.user;

import org.fasttrackit.input.UserInput;

public class BankCustomer extends Person {
    private final int id;
    private BankAccount bankAccount;
    private boolean isActive;
    public BankAccount getBankAccount() {
        return this.bankAccount;
    }

    public void setAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public boolean validatePin(String inputPin) {
        return bankAccount.getCard().verifyPin(inputPin);
    }

    public BankCustomer(int bId, String cnp, String firstName, String lastName) {
        super(cnp, firstName, lastName);
        this.id = bId;
        this.isActive = true;
    }
    public void deactivateAccount(){
        this.isActive = false;
    }

    public short parseInput(String input) {
        Short.valueOf(input);
        System.out.println("input byte is: " + input);
        // Convert string to byte
        // using parseByte() method
        return 0;
    }

    public String selectOptionMenu() {
        String option = UserInput.readFromKeyboard();
        System.out.println("Person: " + getFirstName() + " selected: " + option);
        return option;
    }

    public boolean validateChangedPin(String changedPin, String confirmChangedPin) {
        return changedPin.equals(confirmChangedPin);
    }
    @Override
    public String getFullName() {
        return getFirstName()+ " "+getLastName();
    }

    public void depositCash(int amount) {
        double balance = bankAccount.getBalance();
        bankAccount.updateBalance(balance+amount);
    }

    public void updatePin(String changedPin) {
        bankAccount.getCard().updatePin(changedPin);
    }
}
