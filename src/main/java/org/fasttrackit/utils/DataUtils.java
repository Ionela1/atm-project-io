package org.fasttrackit.utils;

import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;
import org.fasttrackit.user.card.Card;

public class DataUtils {
    public static BankCustomer getCustomer() {
        BankCustomer customer = new BankCustomer(2, "2850990323981","Rayan", "Mihai");
        //bankCustomer.setMiddleName("calin");
        BankAccount account = new BankAccount("RO00TTNC01019185", "RON");
        account.updateBalance(150);
        Card card = new Card("1234567890123456","3838");
        account.setCard(card);
        customer.setAccount(account);
        return customer;
    }
}
