package org.fasttrackit.user;

import org.fasttrackit.user.card.Card;

public class BankAccount {
    private final String accountNumber;
    private Card card;
    private final String currency;

    public double getBalance() {
        return balance;
    }

    public void updateBalance(double balance) {
        this.balance = balance;
    }

    private double balance = 0;

    public BankAccount(String accountNumber, String currency) {
        this.accountNumber = accountNumber;
        this.currency = currency;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getCurrency() {
        return this.currency;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
}
