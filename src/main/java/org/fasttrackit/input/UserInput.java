package org.fasttrackit.input;

import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;

import java.io.InputStream;
import java.util.Scanner;

public class UserInput {
    public static String readFromKeyboard() {
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        return keyboard.next();
    }
    public static boolean authenticate(BankCustomer someone) {
        for (int i = 0; i < 3; i++) {
            String pinCode = Display.askForPin();
            boolean isValid = someone.validatePin(pinCode);
            if (isValid) {
                return true;
            }
            Display.displayInvalidPinMsg();
        }
        return false;
    }

    public static int readIntFromKeyboard() {
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        return keyboard.nextInt();
    }
}


