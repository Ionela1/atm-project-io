package org.fasttrackit.ui;

import org.fasttrackit.employee.Employee;
import org.fasttrackit.input.UserInput;
import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;
import org.fasttrackit.user.card.Card;

import static org.fasttrackit.ATMApp.ATM_NAME;

public class Display {
    public static final String APP_MENU =
            """
            Select an option:
            1.Withdraw cash
            2.Check balance
            3.Change PIN;
            4.Activate card
            5.Depposit cash
            6.Deactivate account
            7.Show IBAN
            8.Show Card details
            """;
    public static void showWelcomeMsg() {
        System.out.println("Welcome to "+ATM_NAME);
    }
    public static String askForPin(){
        System.out.println("Please enter your pin number:");
        return readFromKeyBoard();
    }
    public static String askForNewPin() {
        System.out.println("Please enter new pin number:");
        return readFromKeyBoard();
    }

    public static String confirmNewPin() {
        System.out.println("Please confirm new pin number:");
        return readFromKeyBoard();
    }
    public static String readFromKeyBoard() {
        return UserInput.readFromKeyboard();
    }

    public static void displayInvalidPinMsg() {
        System.out.println("Pin incorrect please try again.");
    }
    public static void showMenu(){
        System.out.println(APP_MENU);
    }
    public static int askUserForAmount(){
        System.out.println("Type int amount you want :");
        return UserInput.readIntFromKeyboard();
    }
    public static void lockUserFor24h(){
        System.out.println("Incorrect pin, card locked for 24 h");
    }
    public static void showCustomerBalanceSheet(BankAccount bankAccount) {
        System.out.println("Your balance is: "+bankAccount.getBalance()+" "+bankAccount.getCurrency());
    }
    public static void showNotEnoughMoneyMsg() {
        System.out.println("Your bank accoutn has not enough money");
    }

    public static void activateCardMessage(BankCustomer someone) {
        System.out.printf("Thank you%s for activating the card.",someone.getFullName());
    }

    public static void informUserWithDeactivation(){
        System.out.println("Thank you for being a valuable customer. Hope you come back.");
    }

    public static void getShowIban(BankCustomer someone) {
        System.out.println("Iban: "+someone.getBankAccount().getAccountNumber());
    }

    public static void showCardDetails(Card card) {
        System.out.println("Card details: "+card.getCardNumber());
    }

    public static void repairAtm(Employee employee) {
        System.out.println("Employee "+ employee.getFullName()+ " is repairing the ATM");
    }
}

