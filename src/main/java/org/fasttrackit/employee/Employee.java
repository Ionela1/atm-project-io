package org.fasttrackit.employee;

import org.fasttrackit.user.Person;

public class Employee extends Person {
    private final String title;

    public Employee(String cnp, String firstName, String lastName, String title) {
        super(cnp, firstName, lastName);
        this.title = title;
    }

    @Override
    public String getFullName() {
        return title + " " + getLastName() + " " + getFirstName();
    }
}
