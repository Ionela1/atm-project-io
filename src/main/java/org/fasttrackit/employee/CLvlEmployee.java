package org.fasttrackit.employee;

public class CLvlEmployee extends Employee{
    private final String signature;
    public CLvlEmployee(String cnp, String firstName, String lastName, String title, String signature) {

        super(cnp, firstName, lastName, title);
        this.signature = signature;
    }

    @Override
    public String getFullName() {
        return signature+"->"+super.getFullName();
    }
}
